tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer i = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e},declarations={$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> plus(x={$e1.st},y={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> minus(x={$e1.st},y={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(x={$e1.st},y={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(x={$e1.st},y={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.hasSymbol($ID.text)}? -> setVar(x={$i1.text},y={$e2.st})
        | ^(EQUAL e1=expr e2=expr) -> equal(x={$e1.st},y={$e2.st})
        | ^(NOT_EQUAL e1=expr e2=expr) -> not_equal(x={$e1.st},y={$e2.st})
        | ^(IF e1=expr e2=expr e3=expr?) {i++;} -> if(cond={$e1.st}, cond_true={$e2.st}, cond_false={$e3.st}, i={i.toString()})
        | INT          -> int(x={$INT.text})
        | ID {globals.hasSymbol($ID.text)}? -> getVar(x={$ID.text})
    ;
    