grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr
    | if_condition NL -> if_condition
//    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)

    | PRINT expr NL -> ^(PRINT expr)
    | NL ->
    | LB
    | RB
    ;

if_condition
    : IF^ if_expr THEN! stat (ELSE! stat)?
    ;
    
if_expr
    : expr
    ( EQUAL^ expr
    | NOT_EQUAL^ expr
    )*
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
     : powExpr
      ( MUL^ powExpr
      | DIV^ powExpr
      )*
    ;
    
powExpr
    : atom
//      ( MUL^ atom
//      | DIV^ atom
      ( POW^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

IF: 'if';

ELSE: 'else';

THEN: 'then';

EQUAL: '==';

NOT_EQUAL: '!=';

PRINT : ('print');

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;

POW
  : '^'

  ;

LB
  : '{'

  ;

RB
  : '}'

  ;