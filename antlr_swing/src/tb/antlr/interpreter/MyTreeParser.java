package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {


    public MyTreeParser(TreeNodeStream input) {

        super(input);

    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {

        super(input, state);

    }

    protected void drukuj(String text) {
    	
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
        
    }

	protected Integer getInt(String text) {
		
		return Integer.parseInt(text);
	}
	
    protected int dzielenie(int x, int y) throws RuntimeException {
		if (y==0) {
			throw new RuntimeException("Nie dzielimy przez 0!");
		}
		return x/y;
	}
    
	protected int potegowanie(int x, int y) {

		if (y==0) return 1;
		int pow=1;
		for(int z=0; z<y; z++)
			pow=pow*x;
		return pow;
	}
	
	private LocalSymbols localSymbols = new LocalSymbols();
	
	protected void createVAR(String x) {

		localSymbols.newSymbol(x);
	}
	
	protected int setVAR(String x, int y) {

		localSymbols.setSymbol(x, y);
		return y;
	}
	
	protected int getID(String x) {

		return localSymbols.getSymbol(x);
	}
	

	protected void enterToScope() {

		localSymbols.enterScope();
	}
	
	protected void leaveFromScope() {

		localSymbols.leaveScope();
	}
}